import {
  GET_TIME,
  BIND_ERROR,
  INVENTORY_BIND_WAREHOUSE,
  GET_VIDEO_URL,
  INVENTORY_BIND_SHARPENING,
  GET_IF_MOUNT,
  CHECK_WAREHOUSE,
  CHECK_WAREHOUSE_ERROR,
  WRITE_OFF,
  WRITE_OFF_ERROR,
  WRITE_OFF_ALREADY_MOUNTED,
  GET_HISTORY,
  WRITE_OFF_ALREADY_WRITTEN_OFF,
  WRITE_OFF_INVENTORY_NOT_EXISTS,
  BIND_ERROR_NOT_FOUND,
} from './types';
import { instance as axios } from '../utils/interceptor';

// let headers
// AsyncStorage.getItem('token').then((token)=>{
//   console.log('this is AsyncStorage token from inventory',token);
//   let headers = {
//     'Content-Type': 'application/json',
//     'Authorization': token
//   };
// });

export const getTime = id => (dispatch) => {
  // axios.get(`inventory/${id}/servedate`)
  //   .then((res) => {
  //     dispatch({
  //       type: GET_TIME,
  //       payload: res.data
  //     });
  //   })
  //   .catch(() => {
  //   });
};

export const bindInventoryToWarehouse = (data) => dispatch => {
  return axios.put( `inventory/inventory-bind-warehouse`, data )
    .then((res) => {
        dispatch({
          type: INVENTORY_BIND_WAREHOUSE,
          payload: res.data
        });
      })
    .catch( ( err ) => {
      // if (err.response && err.response.status !== 404) {
// TODO DISPATCH NEW ACTION THAT MEANS INVENTORY NOT EXIST
//       } else {
        dispatch( {
          type: BIND_ERROR,
          payload: err.response
        } );
      // }
      return Promise.reject( err );
    } )
};

export const bindInventoryToSharpening = (data) => dispatch => {
  return axios.put( `inventory/inventory-bind-repair`, data)
    .then((res) => {
        dispatch({
          type: INVENTORY_BIND_SHARPENING,
          payload: res.data
        });
      })
//     .catch( ( err ) => {
//       // if (err.response && err.response.status !== 404) {
// // TODO DISPATCH NEW ACTION THAT MEANS INVENTORY NOT EXIST
// //       } else {
//         dispatch( {
//           type: BIND_ERROR,
//           payload: err.response
//         } );
//       // }
//       return Promise.reject( err );
//     } )


    .catch( ( err ) => {
      if (err.response && err.response.status !== 404) {
        console.log('eeeeeeeeeeeeeeeeeeeeeeeeeeeee', err.response, 'dddd', err);
        dispatch( {
          type: BIND_ERROR,
          payload: err.response
        } );
      }else if(err.response && err.response.message !== 'Not found warehouse or repair.'){
        dispatch( {
          type: BIND_ERROR_NOT_FOUND,
          payload: err.response
        } );
      }

      return Promise.reject( err );
    } )
};

export const checkWarehouse = (id) => dispatch => {
  return axios.get(`warehouse/${id}` )
    .then((res) => {
        dispatch({
          type: CHECK_WAREHOUSE,
          payload: res.data
        });
      })
    .catch( ( err ) => {
      dispatch( {
        type: CHECK_WAREHOUSE_ERROR,
        payload: err.response
      } );
      return Promise.reject( err );
    } )
};

export const writeOff = (id,data) => dispatch => {
  // console.log("this is from writeOff",id,data)
  return axios.put(`inventory/ban/${id}`,data )
    .then((res) => {
        dispatch({
          type: WRITE_OFF,
          payload: res.data
        });
      })
    .catch( ( err ) => {
      if(err.response && err.response.error === 'Inventory is utilize.'){
        // console.log("this is from writeOff",id,data);
        dispatch( {
          type: WRITE_OFF_ALREADY_WRITTEN_OFF,
          payload: err.response
        } );
      }
      else if(err.response && err.response.status === 404) {
      // else if(err.response && err.response.error === 'Not Found Item') {
        console.log("this is from writeOff",id,data);
        dispatch( {
          type: WRITE_OFF_INVENTORY_NOT_EXISTS,
          payload: err.response,
        } );
      }
      else {
        dispatch( {
          type: WRITE_OFF_ERROR,
          payload: err.response
        } );
      }
      return Promise.reject( err );
    } )
};


export const getManualUrl = () => dispatch => {
  return axios.get( `/video-url` )
    .then((res) => {
      dispatch({
        type: GET_VIDEO_URL,
        payload: res.data
      });
    })
    .catch( ( err ) => {
      dispatch( {
        type: BIND_ERROR,
        payload: err.response
      } );
      return Promise.reject( err );
    } )
};

export const getHistory = (id, limit, offset) => dispatch => {
  return axios.get( `/inventory/${id}/transactions?limit=${limit}&offset=${offset}` )
    .then((res) => {
      dispatch({
        type: GET_HISTORY,
        payload: res.data
      });
    })
    .catch( ( err ) => {
      dispatch( {
        // type: BIND_ERROR,
        // payload: err.response
      } );
      return Promise.reject( err );
    } )
};

export const checkIfMount = (id) => dispatch => {
  return axios.get( `/status/${id}` )
    .then((res) => {
      dispatch({
        type: GET_IF_MOUNT,
        payload: res.data
      });
      return res;
    })
    .catch( ( err ) => {
      dispatch( {
        // type: BIND_ERROR,
        // payload: err.response
      } );
      return Promise.reject( err );
    } )
};


