import { CLOSE_WINDOW } from './types';

export const closeError = () => (dispatch) => {
  dispatch({
    type: CLOSE_WINDOW
  });
};
export default null;
