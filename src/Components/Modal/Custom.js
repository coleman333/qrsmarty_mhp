import React, { Component } from 'react';
import {
  View, Text, TextInput
} from 'react-native';

import Svg, { G, Path } from 'react-native-svg';
import PropTypes from 'prop-types';
import styles from './styles';
import { Button } from '../Button';
import Colors from '../../Constants/Colors';

export default class Custom extends Component {
  constructor (props) {
    super(props);
    this.state = {
      id: ''
    };
    this.confirm = this.confirm.bind(this);
    this.getValue = this.getValue.bind(this);
  }

  getValue (e) {
    const { onChange } = this.props;
    if (onChange) onChange(e);
    this.setState({
      id: e
    });
  }

  confirm () {
    const { onClick } = this.props;
    const { id } = this.state;
    onClick(id.toLowerCase());
  }

  render () {
    const { children, placeholder } = this.props;
    return (
      <View
        style={styles.prompt}
      >
        <View
          style={styles.imageBlock}
        >
          <Svg viewBox="0 0 52 52" width="90" height="90">
            <G>
              <Path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" fill="#e47b7b" />
              <Path d="M26,10c-0.552,0-1,0.447-1,1v22c0,0.553,0.448,1,1,1s1-0.447,1-1V11C27,10.447,26.552,10,26,10z" fill="#e47b7b" />
              <Path d="M26,37c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C27,37.447,26.552,37,26,37z" fill="#e47b7b" />
            </G>
          </Svg>
        </View>
        <View
          style={styles.textBlock}
        >
          <Text
            style={styles.text}
          >
            {children}
          </Text>
        </View>
        <View
          style={{ flex: 2 }}
        >
          <TextInput
            onChangeText={this.getValue}
            placeholder={placeholder}
            style={styles.input}
          />
        </View>
        <View
          style={styles.buttonBlock}
        >
          <Button
            onClick={this.confirm}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.success
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            OK
          </Button>
        </View>
      </View>
    );
  }
}

Custom.defaultProps = {
  onClick () {

  },
  onChange () {

  },
  children: '',
  placeholder: ''
};

Custom.propTypes = {
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  children: PropTypes.string,
  placeholder: PropTypes.string
};
