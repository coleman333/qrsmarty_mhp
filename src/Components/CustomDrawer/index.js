import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { DrawerItems, StackActions, NavigationActions } from 'react-navigation';

export default props => (
  <SafeAreaView style={{ flex: 1 }}>
    <ScrollView>
      <DrawerItems
        {...props}
        onItemPress={
          (router) => {
            console.log('new screen');
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: router.route.routeName })]
            });
            const navigateAction = NavigationActions.navigate({
              routeName: router.route.routeName
            });
            props.navigation.dispatch(navigateAction);
            props.navigation.dispatch(resetAction);
          }
        }
      />
    </ScrollView>
  </SafeAreaView>
);
