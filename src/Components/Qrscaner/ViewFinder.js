import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

export default class Viewfinder extends Component {
  constructor (props) {
    super(props);
    this.getBackgroundColor = this.getBackgroundColor.bind(this);
    this.getSizeStyles = this.getSizeStyles.bind(this);
    this.getEdgeSizeStyles = this.getEdgeSizeStyles.bind(this);
  }

  getBackgroundColor () {
    const { backgroundColor } = this.props;
    return ({
      backgroundColor
    });
  }

  getSizeStyles () {
    const { height, width } = this.props;
    return ({
      height,
      width
    });
  }

  getEdgeSizeStyles () {
    const { borderLength } = this.props;
    return ({
      height: borderLength,
      width: borderLength
    });
  }

  render () {
    return (
      <View
        style={{
          flex: 1,
          borderColor: 'rgba(0,0,0,.7)',
          borderWidth: 60,
          borderTopWidth: 180,
          borderBottomWidth: 180
        }}
      />
    );
  }
}

Viewfinder.defaultProps = {
  backgroundColor: 'transparent',
  borderLength: 30,
  height: 200,
  width: 200
};

Viewfinder.propTypes = {
  backgroundColor: PropTypes.string,
  borderLength: PropTypes.number,
  height: PropTypes.number,
  width: PropTypes.number
};
