import { StyleSheet } from 'react-native';
import { fontSize } from '../../Constants/Text';
import colors from '../../Constants/Colors';
import others from '../../Constants/Other';

export default StyleSheet.create({
  input: {
    flex: 5,
    height: 40,
    textAlign: 'center',
    paddingHorizontal: 20,
    fontSize: fontSize.small,
    color: colors.gray,
    justifyContent: 'center'
  },
  button: {
    flex: 3
  },
  inputWithButton: {
    marginVertical: 10,
    borderColor: colors.grayBorder,
    borderWidth: 1,
    borderRadius: 20,
    height: 40,
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginBottom: 20
  },
  item: {
    height: 40,
    marginVertical: 5,
    color: colors.gray,
    borderWidth: 1,
    padding: 5,
    borderRadius: others.borderRadius,
    borderColor: colors.grayBorder,
    justifyContent: 'center'
  },
  itemText: {
    color: colors.gray,
    textAlign: 'center'
  }
});
