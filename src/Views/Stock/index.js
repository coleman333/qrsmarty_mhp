import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import { RNCamera as Camera} from 'react-native-camera';//  todo: maybe change library to new
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';

import styles from './styles';
import ViewFinder from './ViewFinder';
import _Modal from '../../Components/Modal';
import Button from '../../Components/Button';

import {
  bindInventoryToWarehouse,
  bindInventoryToSharpening,
  checkWarehouse,
  checkIfMount
} from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import { searchAction } from '../../../actions/searchAction';
import Spinner from '../../Components/ActivityIndicator';
// import Button from "../../Components/Qrscaner";


export class Stock extends Component {

  state = {
    stock: true,
    stockId: null,
    nextWindow: false,
    stockError: false,
    bindError: false,
    successModal: false,
    scan: true,
    mac: null,
    // modal: false,
    enableButtonLocal: true,
    // confirmResolveWindow: true
  };


  componentDidMount() {
    DeviceInfo.getMACAddress().then( mac => {
      this.setState( {
        mac: mac
      } );
    } );
    this.setState( { page: this.props.navigation.state.routeName } );
    // this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
    //   this.goBack();
    //   return true;
    // });
    if ( this.props.navigation.state.params.warehouse_id ) {
      this.setState( { stock: false, stockId: this.props.navigation.state.params.warehouse_id }, () => {
      } )
    }
  }

  componentWillUnmount() {
    this.setState( {
      stock: true,
      scan: true,
      page: ''
    } );
    // this.backHandler.remove();
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      bindError: nextProps.bindError,
      // scan: !nextProps.bindError, //  camera waits resolving window
      // page: nextProps.navigation.state.params.page,
      checkWarehouseError: nextProps.checkWarehouseError,
      checkWarehouseResult: nextProps.checkWarehouseResult,// if warehouse exist
      ifMount: nextProps.ifMount,
      modal: nextProps.error,
    } );
    if( nextProps.inventory && nextProps.inventory.is_ban){
      this.setState({is_ban: nextProps.inventory.is_ban})
    }
    if( nextProps.bindErrorNotFound ){
      this.setState({bindErrorNotFound: nextProps.bindErrorNotFound})
    }
      console.log('hhhhhhhhhhhhhhhhhhhhhh', nextProps)
    // if ( this.props.inventory && this.props.inventory.is_ban !== false ) {
    //   this.setState( { confirmStockSharpening: true } );
    // }
  };

  getStock = async ( id ) => {
    if(id!== ''){
      id = id.toLowerCase();
    }else{
      return null
    }

    this.setState( {
      stockId: id,
      inProgress: true,
    } );
    switch ( id[0] ) {
      case 'w':
      {
        console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
        if(this.props.navigation.state.routeName === 'Stock'){
          await this.props.checkWarehouse( id );
          this.setState({successModal: true})
        }else{
          console.log('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');
          this.setState({checkWarehouseError: true});
        }
      }
      break;
      case 'r':
        {
          console.log('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
          if(this.props.navigation.state.routeName === 'Sharpening'){
            console.log('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk',this.props.navigation.state.routeName);

            await this.props.checkWarehouse( id );
            this.setState({successModal: true})
          }else{
            console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb');
            this.setState({checkWarehouseError: true});
          }
        }
        break;
      default:{
        this.setState({checkWarehouseError: true});
      }
      break;
    }
    this.setState( {

      scan: false,        //true
      stock: false,
      inProgress: false,
      // enableButtonLocal: false,
    } );
  };

  resolveSuccessModal = () => {
    this.setState( {
      successModal: false,
      stock: false,
      scan: true
      // scan: false

    } );
  };


  // bindInventory = ( id ) => {
  //   id = id.toLowerCase();
  //   this.setState({stockId:this.state.stockId.toLowerCase(),inventoryId:id}) ;
  //   // console.log('this from bindInventory=========',this.state.is_ban )
  //   this.props.searchAction( this.state.inventoryId )
  //     .then( () => {
  //       if ( this.props.inventory && this.props.inventory.is_ban !== false ) {
  //         this.setState( { confirmStockSharpening: true } );
  //       }else{
  //         this.bindInventoryStockSharpening()
  //       }
  //     } );
  // };

  bindInventory = ( id ) => {
    if(id !== '' && id !== undefined){
      id = id.toLowerCase();
    }else{
      return null
    }
    this.setState({stockId:this.state.stockId.toLowerCase(), inventoryId:id, inProgress: true,}) ;
    // this.props.searchAction( this.state.inventoryId, 'identify_id' )
    const type = 'identify_id';
    this.props.searchAction( this.state.inventoryId, type, this.props.navigation)
      .then( () => {
        // if(this.state.modal){
        //   this.setState({modal: true})
        // }
        this.props.checkIfMount(this.state.inventoryId)
          .then(()=>{
            if ( this.props.inventory && this.props.inventory.is_ban !== false && this.state.ifMount.data[0].is_mounted === false ) {
              this.setState( { confirmStockSharpening: true, inProgress: false, } );
            }else if(this.state.ifMount.data[0].is_mounted){
              this.setState({isMountedRestrictionWindow:true, inProgress: false, })
            }else{
              this.setState( { inProgress: true, } );
              this.bindInventoryStockSharpening()
            }
          })
      } );
  };

  confirmedBindInventory = () => {
    // if ( this.state.confirmResolveWindow !== false ) {                //if confirm window result with positive result
    //   this.setState({confirmResolveWindow: false })
      // this.openNextWindow();

      this.setState({ scan: true } );
      this.bindInventoryStockSharpening();
    // }
  };

  bindInventoryStockSharpening = () =>{
    if ( this.state.page && this.state.page === 'Sharpening' ) {
      this.props.bindInventoryToSharpening( {
        inventory_id: this.state.inventoryId,
        warehouse_id: this.state.stockId,
        username: this.state.mac
      } )
        .then( () => {
          // if(this.state.bindError === false){
          this.setState( { inProgress: false, } );
            this.openNextWindow();
          // }
        } )
        .catch( err => {
          console.log( err )
        } )
    } else if ( this.state.page && this.state.page === 'Stock' ) {
      this.props.bindInventoryToWarehouse( {
        inventory_id: this.state.inventoryId,
        warehouse_id: this.state.stockId,
        username: this.state.mac
      } )
        .then( () => {
          // if(this.state.bindError === false){
          this.setState( { inProgress: false, } );
            this.openNextWindow();
          // }
        } )
        .catch( err => {
          console.log( err )
        } )
    }
  };

  openNextWindow = () => {
    this.setState( {
      nextWindow: true,
      scan: false
    } );
  };

  resolveNextWindow = () => {
    this.setState( {
      nextWindow: false,
      scan: true
    } );
  };

  resolveconfirmStockSharpening = () => {
    this.setState( {
      confirmStockSharpening: false,
      confirmResolveWindow: true,
      // nextWindow: true,
      scan: true
    } );
    // this.confirmedBindInventory();
    this.bindInventoryStockSharpening()
  };

  rejectNextWindow = () => {
    // this.goBack();
    this.props.navigation.navigate( 'Identification' );
    this.setState( {
      confirmStockSharpening:false,
      nextWindow: false,
      scan: true,
      stock: true,
      inProgress: false,
    } );
  };

  closeModal = () => {
    this.props.closeError();
    this.setState( {
      scan: true,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
      // stock: true
      inProgress: false,
    } );
  };

  closeWrongWarehouseModal = ()=> {
    this.props.closeError();
    this.setState( {
      scan: true,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
      // stock: true
      inProgress: false,
    } );
    if(this.state.page === 'Stock'){
      this.props.navigation.navigate('CommonPageWarehouse');
    }else{
      this.props.navigation.navigate('CommonPageSharpening');
    }

  };

  bindErrorNotFoundCloseModal = () => {
    this.props.closeError();
    this.setState( {
      scan: false,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
      // stock: true
    } );
    if(this.state.page !== 'CommonPageWarehouse'){
      this.props.navigation.navigate('CommonPageWarehouse');
    }else{
      this.props.navigation.navigate('CommonPageSharpening');
    }
  };

  turnFlash = () => {
    // if ( this.state.torch === Camera.constants.TorchMode.on ) {
    //   this.setState( { torch: Camera.constants.TorchMode.off, flash: Camera.constants.FlashMode.off } )
    // } else {
    //   this.setState( { torch: Camera.constants.TorchMode.on, flash: Camera.constants.FlashMode.on } )
    // }

    if ( this.state.torch === Camera.Constants.FlashMode.torch ) {
      // this.setState( { torch: Camera.Constants.TorchMode.off, flash: Camera.Constants.FlashMode.off } )
      this.setState( { torch: Camera.Constants.FlashMode.off, flash: Camera.Constants.FlashMode.off } )
    } else {
      // this.setState( { torch: Camera.Constants.TorchMode.on, flash: Camera.Constants.FlashMode.on } )
      this.setState( { torch: Camera.Constants.FlashMode.torch, flash: Camera.Constants.FlashMode.torch } )
    }
  };

  refusedIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
    } );
  }

  confirmIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
      isMount: false,
    },this.confirmedBindInventory() );
  }

  showManualEnterWindow(){
    this.setState({ manualEnterWindow:true});
  }

  onChangeInventory(e){
    this.setState({inventoryId: e.trim()});
  }

  bindWarehouseManually = () => {
    this.bindInventory(this.state.inventoryId);
    this.setState({manualEnterWindow: false})
  };

  render() {
    // let { flash, torch } = this.state;
    let page;
    if ( this.props.navigation.state ) {
      page = this.props.navigation.state.routeName;
    }

    return (
      <View
        style={ { flex: 1 } }
      >
        <_Modal
          placeholder={ 'Введите Идентификатор' }
          onChange={ this.onChangeInventory.bind(this) }
          visible={this.state.manualEnterWindow}
          type="custom"
          closeModal={this.bindWarehouseManually}
        >
          Введите идентификатор ТМЦ Вручную
        </_Modal>
        <_Modal
          closeModal={this.closeModal}
          visible={this.state.modal}
          type="alert"
        >
          {`Идентификатор ${this.state.inventoryId} не найден в базе!`}
        </_Modal>

        <_Modal
          closeModal={this.bindErrorNotFoundCloseModal}
          visible={this.state.bindErrorNotFound}
          type="alert"
        >
          {/*{`Идентификатор ${this.state.inventoryId} не найден в базе!`}*/}
          {`Не найден участок заточки`}
        </_Modal>

        <_Modal
          closeModal={ this.refusedIsMountedRestrictionWindow.bind(this) }
          confirmModal={ this.confirmIsMountedRestrictionWindow.bind(this) }
          visible={ this.state.isMountedRestrictionWindow }
          type="confirm"
        >
          {
            `Внимание, данное ТМЦ установлено на оборудование. В случае осуществления вами операции, оно будет демонтировано автоматически. Продолжить?`
          }
        </_Modal>
        <_Modal
          closeModal={ this.closeModal }
          visible={ this.state.bindError }
          type={ 'alert' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Деталь уже находится на данном складе' : ' Деталь уже находится на участке заточки' }
        </_Modal>
        <_Modal
          closeModal={ this.rejectNextWindow }
          confirmModal={ this.resolveconfirmStockSharpening }
          visible={ this.state.confirmStockSharpening }
          type={ 'confirm' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Деталь уже списана с производства. Хотите продолжить' : 'Деталь уже списана с производства. Хотите продолжить' }
        </_Modal>
        <_Modal
          closeModal={ this.rejectNextWindow }
          confirmModal={ this.resolveNextWindow }
          visible={ this.state.nextWindow }
          type={ 'successConfirm' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Операция Успешна!\r\nХотите ли принять другую деталь на склад?' :
            'Операция Успешна!\r\nХотите ли принять другую деталь на участок заточки?'
          }
        </_Modal>
        <_Modal
          closeModal={ this.resolveSuccessModal }
          visible={ this.state.successModal }
          type={ 'success' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Склад определен' :
            'Участок заточки определен'
          }
        </_Modal>
        <_Modal
          closeModal={ this.closeWrongWarehouseModal }
          visible={ this.state.checkWarehouseError }
          type={ 'alert' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Такого склада не существует' :
            'Такого участка заточки не существует'
          }
        </_Modal>
        { this.state.inProgress ? <Spinner/> :

          <Camera
            // flashMode = {this.state.flash}
            flashMode={ this.state.torch }
            style={ { flex: 1 } }
            onBarCodeRead={ ( { data } ) => {
              if ( this.state.scan ) {
                this.setState( { scan: false } );
                if ( this.state.stock ) {
                  this.getStock( data );
                } else {
                  this.bindInventory( data );
                }
              }
            } }
          >
            { Platform.OS === 'ios' && <ViewFinder/> }

            <React.Fragment>

              {/*<View style={ styles.manualEnterButton }>*/ }
              {/*<TouchableOpacity onPress={ this.showManualEnterWindow.bind(this) } style={ styles.stockButton }>*/ }
              {/*<Text style={ styles.stockButtonStyle }> Ввести код вручную </Text>*/ }
              {/*</TouchableOpacity>*/ }
              {/*</View>*/ }
              {/*<View style={ styles.lightButton }>*/ }
              {/*<TouchableOpacity onPress={ this.turnFlash } style={ styles.stockButton }>*/ }
              {/*<Text style={ styles.stockButtonStyle }> освещение </Text>*/ }
              {/*</TouchableOpacity>*/ }
              {/*</View>*/ }

              <View style={ styles.buttonsContainer }>

                <View style={ styles.buttonContainer }>

                  { this.props.navigation.state.params.warehouse_id && <Button
                    // enableButton={this.state.enableButtonLocal}
                    onClick={ this.showManualEnterWindow.bind( this ) }> Ввести код вручную
                  </Button> }
                </View>

                <View style={ styles.buttonContainer }>
                  <Button onClick={ this.turnFlash }> освещение </Button>
                </View>

              </View>

              { Platform.OS === 'android' && <ViewFinder/> }

              <View style={ styles.header }>
                <Text style={ styles.headerText }>
                  {
                    this.state.stock ?
                      (this.state.page && this.state.page !== 'Sharpening' ? `Отсканируйте QR-код склада` : `Отсканируйте QR-код участка заточкии`)
                      : `Отсканируйте QR-код ТМЦ`
                  }
                </Text>
              </View>
            </React.Fragment>
          </Camera>
        }
      </View>
    )
  }
}

const mapStateToProps = ( state ) => ({
  bindError: state.error.errors.bindError,
  checkWarehouseError: state.error.errors.checkWarehouseError,
  // checkWarehouseResult: state.inventory.checkWarehouseResult
  inventory: state.inventory.inventory,
  ifMount: state.inventory.ifMount,
  error: state.error.errors.identifyIdError,
  bindErrorNotFound: state.error.errors.bindErrorNotFound,
});

export default connect( mapStateToProps, {
  checkWarehouse, bindInventoryToSharpening, bindInventoryToWarehouse, checkIfMount,
  closeError, searchAction
} )( Stock )
